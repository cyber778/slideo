export function getSettings(){
  return {

    // used for text
    style: {
      fontSize: '20px',
      color: '#333',
      fontFamily: 'Arial',
      stroke: '#fff',
      backgroundColor: '#3311aa',
      strokeThickness: 3,
    },

    //game settings
    config: {
      type: Phaser.AUTO,
      width: 640,
      height: 480,
      scene: {
        preload: preload,
        create: create,
        update: update
      },
      backgroundColor: '#fff'
    }


  }
}
