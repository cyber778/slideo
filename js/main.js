/*
 * Autor: Elad Silberring (yours truely)
 * Slideshow-to-Video Implamentation Based On Phaserjs 3
 *
 * */

// GLOBALS immutable


const SETTINGS = {
  gameConfig: {
    parent: 'slideo',
    type: Phaser.AUTO,
    width: 640,
    height: 400,
    scene: {
      preload: preload,
      create: create,
      update: update
    },
    backgroundColor: '#e7e7e7'
  },
  imageScale: 1.15,
  textBgWidth: 400,
  textBgHeight: 120,
  sceneDuration: 3000,
  textDefaultStyle: {
    fontSize: '22px',
    color: '#fff',
    fontFamily: 'Montserrat',
    stroke: '#333',
    strokeThickness: 2.5,
    align: "center",
    shadowFill: true,
    shadowBlur: 30,
    shadowColor: "#000"
  }
};

const BG_WIDTH = SETTINGS.textBgWidth;
const BG_HEIGHT = SETTINGS.textBgHeight;
const SCENE_DURATION = SETTINGS.sceneDuration; //time in miliseconds per slide
const config = SETTINGS.gameConfig;

// global dynamic vars
let textObj;

let currentImage;
let currentSize;
let slideCounter = 0;

let timedEvent;
let timedAnimationEvent;
let imageAnimationNumber = 0;
let graphics;

// TODO: remove need for imageName and add default style
// DATA
// let style = {
//   fontSize: '22px',
//   color: '#fff',
//   fontFamily: 'Montserrat',
//   stroke: '#333',
//   strokeThickness: 2.5,
//   align: "center",
//   // shadowFill: true,
//   // shadowBlur: 20,
//
// };
//
// const SLIDES = [
//   {
//     imageName: 'civic1',
//     imageUrl:'img/civic1.jpg',
//     text: 'The first Civic is just not any civic by all means',
//     style
//   },
//   {
//     imageName: 'civic2',
//     imageUrl:'img/civic2.jpg',
//     text: 'The Second Civic',
//     style
//   },
//   {
//     imageName: 'civic3',
//     imageUrl:'img/civic3.jpg',
//     text: 'The Third Civic',
//     style
//   }
// ];


const IMAGE_ANIMATIONS = [
  zoomInAnimation,
  zoomOutAnimation
];

const TEXT_ANIMATIONS = [
  textScaleAnimation,
  textSlideRightAnimation,
  textSlideLeftAnimation
];

let game = new Phaser.Game(config);

function preload() {
  for (slide of SLIDES){
    this.load.image(slide.imageName, slide.imageUrl);
  }
  this.load.image('logo', SLIDE_LOGO.imageUrl);
}


function create() {
  logoImage = this.add.image(0, 0, 'logo').setOrigin(0, 0);
  logoImage.setDisplaySize(SETTINGS.gameConfig.width,SETTINGS.gameConfig.height);
  graphics = this.add.graphics();
  graphics.depth = 2;
  nextSlide.apply(this, [0]);
  timedEvent = this.time.addEvent({
    delay: SCENE_DURATION,
    callback: timedEventHandler,
    callbackScope: this,
    // the "-2" is because we need to remove 1 since we made the first "nextSlide" call already before this timer
    // the other 1 we substract is because the "repeat" should receive n-1
    repeat: SLIDES.length - 2
  });
}

function update() {

}


function textScaleAnimation() {
  currentSize = parseInt(textObj.style.fontSize);
  if (currentSize < 50) {
    currentSize += 1;
    style.fontSize = currentSize + 'px';
    textObj.setStyle(style);
  }
}

function textSlideAnimation(direction) {
  let inBounds = (textObj.x + textObj.width - config.width > 0 || textObj.x >= 0);
  if (direction == "right" && inBounds)
    textObj.x += 2;
  textObj.y += 1;
}

function textSlideLeftAnimation() {
  textSlideAnimation("right");
}

function textSlideRightAnimation() {
  textSlideAnimation("right");
}


function zoomInAnimation() {
  currentImage.scaleX += 0.0002;
  currentImage.scaleY += 0.0002;
}

function zoomOutAnimation() {
  currentImage.scaleX -= 0.0002;
  currentImage.scaleY -= 0.0002;
}


function animationEvent() {
  let counter = timedAnimationEvent.repeat - timedAnimationEvent.repeatCount;
  if (counter == 0) {
    imageAnimationNumber = Math.floor(Math.random() * IMAGE_ANIMATIONS.length);
  }

  // last counter and last slide finishing effect...
  else if (counter == timedAnimationEvent.repeat-1 && slideCounter == SLIDES.length){
    // last loop
    this.tweens.add({
      targets: [currentImage, textObj],
      alpha:0,
      duration:1000
    });
  }

  // TEXT_ANIMATIONS[textAnimationNumber]();
  IMAGE_ANIMATIONS[imageAnimationNumber]();
}

function slideVerticle(currentSlide, side){
  // must apply the `this` to make this function work

  let is_up = (side == "up");

  let startTextXDown = 20;
  let startTextYDown = 20;
  let startTextXUp = (config.width / 2) - (BG_WIDTH / 2) + 50;
  let startTextYUp = config.height - BG_HEIGHT + 20;
  let startGraphicsXUp = (config.width / 2) - (BG_WIDTH / 2);
  let startGraphicsYUp = config.height;
  let startGraphicsYDown = -BG_HEIGHT;
  let startGraphicsXDown = 0;


  let startTextX = (is_up)? startTextXUp : startTextXDown;
  let startTextY = (is_up)? startTextYUp: startTextYDown;
  let startGraphicsX = (is_up)?  startGraphicsXUp: startGraphicsXDown;
  let startGraphicsY = (is_up)?  startGraphicsYUp: startGraphicsYDown;
  let endGraphicsY = (is_up)? -BG_HEIGHT : BG_HEIGHT;
  let endGraphicsY2 = (is_up)? -10 : 10;

  graphics.clear();
  textObj = this.add.text(startTextX, startTextY, currentSlide.text, currentSlide.style);
  textObj.setWordWrapWidth(BG_WIDTH - 50);
  textObj.setDepth(3);
  textObj.alpha = 0;

  // fill(color,alpha)
  graphics.fillStyle(0xa2a3cc);
  graphics.fillRect(startGraphicsX, startGraphicsY, BG_WIDTH, BG_HEIGHT);

  let _this = this;
  this.tweens.add({
    targets: graphics,
    y: endGraphicsY,
    duration: 200,
    onComplete: function () {
      _this.tweens.add({
        targets: graphics,
        alpha: 0.8,
        duration: 400,
        y: endGraphicsY2
      });
    },
  });

  this.tweens.add({
    targets: textObj,
    alpha: 1,
    ease: 'Sine.easeInOut',
    duration: 300,
    delay: 150,
  });

}

function horizontalSlide(currentSlide){
  // set direction
  is_right = true; // (direction=="right");

  // TODO: add directions
  // let startTextXRight = 20;
  // let startTextYRight = 20;
  // let startTextXLeft = (config.width / 2) - (BG_WIDTH / 2) + 50;
  // let startTextYLeft = config.height - BG_HEIGHT + 20;
  // let startGraphicsXLeft = (config.width / 2) - (BG_WIDTH / 2);
  // let startGraphicsYLeft = config.height;
  // let startGraphicsYRight = -BG_HEIGHT;
  // let startGraphicsXRight = 0;
  //
  // let startTextX = (is_right)? startTextXRight : startTextXLeft;
  // let startTextY = (is_right)? startTextYRight: startTextYLeft;
  // let startGraphicsX = (is_right)?  startGraphicsXRight: startGraphicsXLeft;
  // let startGraphicsY = (is_right)?  startGraphicsYRight: startGraphicsYLeft;
  // let endGraphicsY = (is_right)? -BG_HEIGHT : BG_HEIGHT;


  graphics.fillStyle(0xffffff);
  graphics.fillRect(0, 0, BG_WIDTH, BG_HEIGHT);
  graphics.setX(config.width + BG_WIDTH);
  graphics.alpha = 1;
  let repeatDuration = 10; // in ms
  let sideSlideEvent = this.time.addEvent({
    delay: repeatDuration,
    callbackScope: this,
    repeat: 1000 / repeatDuration,
    callback: () => {
      let i = sideSlideEvent.repeat - sideSlideEvent.repeatCount; // number of loops done
      this.tweens.add({
        targets: graphics,
        x: 0,
        duration: 300,
      });
      if ((BG_WIDTH - i*10) > 1) {
        graphics.clear();
        graphics.fillStyle(0xffffff);
        graphics.fillRect(i * 7, 0, BG_WIDTH - i * 10, BG_HEIGHT);
      }
      if(sideSlideEvent.repeatCount==0){
        this.tweens.add({
        targets: graphics,
        alpha: 0,
        duration: 300,
        delay: 700,
      });
      }
    },
  });
  textObj = this.add.text(config.width - 350 + 20, 20, currentSlide.text, currentSlide.style);
  textObj.setWordWrapWidth(BG_WIDTH - 50);
  textObj.setDepth(3);
  textObj.alpha = 0;
  this.tweens.add({
    targets: textObj,
    alpha: 1,
    ease: 'Sine.easeInOut',
    duration: 300,
    delay: 150,
  });
}


function timedEventHandler() {
  // we add the "+1" since the first time this is actually called in the "update" func
  let counter = slideCounter;//timedEvent.repeat - timedEvent.repeatCount + 1;
  nextSlide.apply(this, [counter]);
}


function nextSlide(counter) {
  let currentSlide = SLIDES[counter];

  if (counter > 0) {
    currentImage.destroy();
    textObj.destroy();
  }

  // random animation direction
  animationDir = (Math.random()>0.49)? 'up' : 'down';

  // random text animation leaning towards verticle
  if (Math.random()>0.30)
    slideVerticle.apply(this, [currentSlide, animationDir]);
  else
    horizontalSlide.apply(this, [currentSlide]);

  // slideVerticle.apply(this, [currentSlide, animationDir]);
  // horizontalSlide.apply(this, [currentSlide]);
  currentImage = this.add.image(0, 0, currentSlide.imageName).setOrigin(0.1, 0.1);
  currentImage.scaleX = SETTINGS.imageScale;
  currentImage.scaleY = SETTINGS.imageScale;

  timedAnimationEvent = this.time.addEvent({
    delay: 10,
    callback: animationEvent,
    callbackScope: this,
    repeat: 1000 * currentImage.scaleX - 1000
  });
  slideCounter += 1;

}

/*
 * timedEvent.repeat = the number of total loops in the event
 * timedEvent.repeatCount = the number of loops remaining
 *
 *
 * text style options = {
 fontFamily: [ 'fontFamily', 'Courier' ],
 fontSize: [ 'fontSize', '16px' ],
 fontStyle: [ 'fontStyle', '' ],
 backgroundColor: [ 'backgroundColor', null ],
 color: [ 'color', '#fff' ],
 stroke: [ 'stroke', '#fff' ],
 strokeThickness: [ 'strokeThickness', 0 ],
 shadowOffsetX: [ 'shadow.offsetX', 0 ],
 shadowOffsetY: [ 'shadow.offsetY', 0 ],
 shadowColor: [ 'shadow.color', '#000' ],
 shadowBlur: [ 'shadow.blur', 0 ],
 shadowStroke: [ 'shadow.stroke', false ],
 shadowFill: [ 'shadow.fill', false ],
 align: [ 'align', 'left' ],
 maxLines: [ 'maxLines', 0 ],
 fixedWidth: [ 'fixedWidth', 0 ],
 fixedHeight: [ 'fixedHeight', 0 ],
 rtl: [ 'rtl', false ],
 testString: [ 'testString', '|MÃ‰qgy' ],
 baselineX: [ 'baselineX', 1.2 ],
 baselineY: [ 'baselineY', 1.4 ],
 wordWrapWidth: [ 'wordWrap.width', null ],
 wordWrapCallback: [ 'wordWrap.callback', null ],
 wordWrapCallbackScope: [ 'wordWrap.callbackScope', null ],
 wordWrapUseAdvanced: [ 'wordWrap.useAdvancedWrap', false ]
 };
 * */


/*
 * animations from slidedemo:
 * 0:05 -> image zooming out/in
 * 0:05 -> text showing center top
 * 0:30 -> image moving from left to right/right to left
 * 0:41 -> text showing from bottom center
 * * animations from slidedemo:
 *
 * 0:22 text + image zooming
 *
 * */


// TODO: clean up and add slidedown function as well as fade-in and randomize colors